'use strict';

module.exports = function(grunt) {

  /*
   * grunt.initConfig
   */
  grunt.initConfig({
    watch: {
      html: {
        files: ['../src/jade/**/*.jade'],
        tasks: ['jade']
      },
      css: {
        files: ['../src/scss/**/*.scss'],
        tasks: ['sass']
      },
      js: {
        files: ['../src/coffee/**/*.coffee'],
        tasks: ['coffee']
      }
    },
    jade: {
      dest: {
        expand: true,
        cwd: '../src/jade',
        src: ['**/*.jade'],
        dest: '../src/html',
        ext: '.html'
      }
    },
    sass: {
      dest: {
        expand: true,
        cwd: '../src/scss',
        src: ['**/*.scss'],
        dest: '../src/css',
        ext: '.css'
      }
    },
    coffee: {
      dest: {
        expand: true,
        cwd: '../src/coffee',
        src: ['**/*.coffee'],
        dest: '../src/js',
        ext: '.js'
      }
    }
  });

  // load
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-coffee');

  // default task
  grunt.registerTask('default', ['watch']);
};