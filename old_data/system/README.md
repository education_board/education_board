##コンパイル面のシステム用フォルダです

###現在の環境
* scss-->css
 * windows : 可能
 * mac : 可能
* jade-->html
 * windows : 可能
 * mac : 可能
* coffee->javascript
 * windows : 可能
 * mac : 可能

##簡易説明
###SCSS→CSS
../src/scss 下のscssファイルが ../src/css 下にコンパイルされてcssになる
###JADE→HTML
../src/jade 下のjadeファイルが ../src/html 下にコンパイルされてhtmlになる
###CoffeeScript→JavaScript
../src/coffee 下のcoffeescriptファイルが ../src/js 下にコンパイルされてjavascriptになる

##自動コンパイル環境の起動
[scss-->cssのみはこちら]()
##windowの場合
1. rubyのインストール(aki:ruby 2.0.0p648)  
[Rubyをインストール／アップデートするには？](http://www.buildinsider.net/language/rubytips/0004)  
バージョンは用相談。
2. sass,compassのインストール
[Sass + Compass の基本導入と設定](https://wp-d.org/2013/01/03/1732/)  
※インストールまでで良い
1. nodeのインストール(aki:node v6.9.1)
[Node.js公式](https://nodejs.org/ja/)  
バージョンは用相談。
2. Gruntのインストール
``` bash:grunt.bush
sudo npm install -g grunt-cli
```
3. 同フォルダのgrstart.batを起動

##macの場合
1. rubyのインストール(aki:ruby 2.0.0p648)  
pipとかbrewとか好きな方法でお願いします
2. sass,compassのインストール
[Sass + Compass の基本導入と設定](https://wp-d.org/2013/01/03/1732/)  
※インストールまでで良い
1. nodeのインストール(aki:node v6.9.1)
[Node.js公式](https://nodejs.org/ja/)  
バージョンは用相談。
2. Gruntのインストール
``` bash:grunt.bush
sudo npm install -g grunt-cli
```
3. 同フォルダのgrstart.commandを起動

###SCSS→CSSのみの場合
1. ruby,sass,compassを上記手順各1. 2.よりインストール
2. * windows: 同フォルダのstart.batを起動
  * mac: 同フォルダのstart.commandを起動

